#include "aux/run_event.h"
#include "aux/aux_card.h"

int main(int argc, char **argv) {

  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("tag", value< std::string >()->default_value("Aux1"), "AUX Name in OKS Configuration")
    ;
  
  positional_options_description p;
  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  } catch ( ... ) { std::cerr << desc << endl; return 1; }
  notify(vm);

  if (vm.count("help") ) { cout << endl <<  desc << endl ; return 0; }

  string aux_name = vm["tag"].as<std::string>();

  cout << "Loading OKS Configuration." << endl;
  ::Configuration *db;
  try {
    db = new ::Configuration("oksconfig:ftk/partitions/FTK.data.xml");
  } catch(daq::config::Generic e) {
    std::cerr << "ERROR: Cannot load OKS partition." << std::endl;
    return -1;
  }

  const daq::ftk::dal::ReadoutModule_PU* settings = db->get<daq::ftk::dal::ReadoutModule_PU>(aux_name.c_str());
  if(settings==0)
    {
      std::cerr << "Invalid tag " << aux_name << "! exiting..." << std::endl;
      return -1;
    }
  cout << "Slot         = " << settings->get_Slot() << endl;

  daq::ftk::aux a(settings->get_Slot(), ""); // create aux 
  sleep(2);
  a.setup(settings);
  sleep(2);
  a.loadAndSendEvent();


  // delete db; // this deletion is fucked up for some reason.

  return 0;

}

