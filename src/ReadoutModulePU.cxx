/*****************************************************************/
/*                                                               */
/*                     ReadoutModule_PU                           */
/*                                                               */
/*****************************************************************/

#include <iostream>
#include <stdlib.h>
#include <sys/syscall.h>
#include <memory>               //shared_ptr
#include <utility>              //pair
#include <ctime>

// OKS, RC and IS
#include "ProcessingUnit/ReadoutModulePU.h"
#include "ProcessingUnit/dal/ReadoutModule_PU.h"
#include "config/Configuration.h"
#include "DFdal/RCD.h"
#include "DFdal/ReadoutConfiguration.h"
#include "RunControl/Common/OnlineServices.h"
#include "is/info.h"
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"
#include "ftkcommon/Utils.h"		//getDFOHNameString

namespace daq
{

    namespace ftk
    {

        ReadoutModule_PU::ReadoutModule_PU():
            m_CanPublish(false),
            m_configuration(0),
            m_AmbStandalone(false),
            m_AuxStandalone(false)
        {
            ERS_LOG("Build timestamp: " << __DATE__ << " " << __TIME__);
            if (daq::ftk::isLoadMode())
            { ERS_LOG("Loading mode"); }
        }

        ReadoutModule_PU::~ReadoutModule_PU() noexcept
        {
        
        }


        void ReadoutModule_PU::setup(DFCountedPointer<ROS::Config> configuration)
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(), "Setup");

            // Get online objects from daq::rc::OnlineServices
            m_ipcpartition      = daq::rc::OnlineServices::instance().getIPCPartition();
            m_appName = daq::rc::OnlineServices::instance().applicationName();
            auto dal_rcdAppl    = daq::rc::OnlineServices::instance().getApplication().cast<daq::df::RCD>();
            Configuration *conf = configuration->getPointer<Configuration>("configurationDB");
            auto readOutConfig  = conf->cast<daq::df::ReadoutConfiguration>( dal_rcdAppl->get_Configuration() );

            // Read some info from the DB
            //const ftk::dal::ReadoutModule_PU * module_dal = conf->get<ftk::dal::ReadoutModule_PU>(configuration->getString("UID"));
            const dal::ReadoutModule_PU * module_dal = conf->get<dal::ReadoutModule_PU>(configuration->getString("UID"));
            m_name	  = module_dal->UID();
            m_slot	  = module_dal->get_Slot();
            m_crate       = module_dal->get_Crate();
            m_dryRun  = module_dal->get_DryRun();
            m_isServerName  = readOutConfig->get_ISServerName();

            // set up the MT implementation
            initialize( m_name, module_dal, BOARD_PU );

            //AMBStandaloneTest
            const dal::AMBStandaloneTest *AMBSATest_dal = module_dal->get_AMBStandaloneTest();
            if (AMBSATest_dal)
            {
                ERS_LOG("StandAlone object defined.. Skipping the AUX code execution.");
                m_AmbStandalone = true;
            }
            else
                ERS_LOG("AMBStandAlone object not defined.. Proceeding with the standard configuration.");

            //AUXStandaloneTest
            if (module_dal->get_SourceRoads() == 0x0)
            {
                ERS_LOG("AUX Road Source is TV.. Skipping the AMB code execution.");
                m_AuxStandalone = true;
            }
            else
                ERS_LOG("AUX Road Source is AMB.. Proceeding with the standard configuration");


            if(m_dryRun)
            { daq::ftk::ftkException e(ERS_HERE, m_name,"ReadoutModule_PU.DryRun option set in DB, IPBus calls with be skipped!" ); 
                ers::warning(e);
            };

            // Creating the AMB object
            if (!m_AuxStandalone|| (m_AuxStandalone && m_AmbStandalone))
            {
                try {
                    ERS_LOG("Creating the AMB object in slot " << std::to_string(m_slot));
                    m_AMB=std::make_unique<AMBoard>(m_slot);
                    m_AMB->RC_setup(module_dal);
                    ERS_LOG("Object created");
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AMB setup", ex);
                    throw e;
                }
            }

            //AUX
            //make and configure AUX Object  
            if(!m_AmbStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                try{
                    ERS_LOG("AUX: Setup | Creating the AUX object in crate " << std::to_string(m_crate) << " slot "  << std::to_string(m_slot));
                    m_aux=std::make_unique<daq::ftk::aux>(m_slot, m_crate);
                    m_aux->setup(module_dal);
                    ERS_LOG("Object created");
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AUX setup", ex);
                    throw e;
                }
            }

            ////////Common part

            // Creating the ISInfo object
            std::string isProvider = m_isServerName + "." + m_name; // Gives problems at configure if not the same isProvider for AUX and AMB.
            // Reason is unclear. --Todd, 2018/4/13
            ERS_LOG("IS: publishing in " << isProvider);
            try 
            { 
                if (!m_AuxStandalone || (m_AuxStandalone && m_AmbStandalone))
                    m_ambslpNamed = std::make_unique<ambslpNamed>( m_ipcpartition, isProvider.c_str()); 
                if (!m_AmbStandalone || (m_AuxStandalone && m_AmbStandalone))
                    m_auxNamed=std::make_unique<auxNamed>( m_ipcpartition, isProvider.c_str() ); 
            } 
            catch( ers::Issue &ex)
            {
                daq::ftk::ISException e(ERS_HERE, "Error while creating ambslpNamed and auxNamed object", ex);  // NB: append the original 
                // exception (ex) to the new one 
                ers::warning(e); 
            }

            // Creating the Histogramming provider
            std::string OHServer("Histogramming"); // TODO: make it configurable
            std::string OHName("FTK_" + m_name);   // Name of the pubblication directory
            ERS_LOG("OH: publishing in " << OHServer << "." << m_name << ", from application " << m_appName);

            if (!m_AuxStandalone || (m_AuxStandalone && m_AmbStandalone))
            {
                try {
                    std::ostringstream oh_name;
                    if (m_slot < 10)
                    {
                        oh_name << "FTK_PU-" << m_crate << "-0" << m_slot << "AMB_common"; 
                    }
                    else
                    {
                        oh_name << "FTK_PU-" << m_crate << "-" << m_slot << "AMB_common";
                    }
                    //previously we were using getDFOHNameString, but format is wrong. Building the provider name using directly m_crate/m_slot
                    m_ohRawProviderAMB=std::make_shared<OHRawProvider<>>( m_ipcpartition , OHServer, oh_name.str());
                    m_ohProvider=std::make_unique<OHRootProvider> ( m_ipcpartition , OHServer, OHName);  // Currently only used for AMB
                }
                catch ( daq::oh::Exception & ex)
                {
                    daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
                    ers::warning(e);  
                }

                //Make  DFReaders
                m_dfReaderAmb=std::make_unique<DataFlowReaderAMB>(m_ohRawProviderAMB, true);
                m_dfReaderAmb->init(m_name + ".AMB");

                /* Construct the histograms */
                //AMB
                m_hlast_lvl1id=std::make_unique<TH1F>("AmbLastL1id:1d",        "Last LVL1ID in the AMB spy buffer",   128, -0.5, 127.5);
                m_hnroads=std::make_unique<TH1F>("AmbNRoads:1d",             "Number of roads per spy buffer", 50, -0.5, 49.5);
                m_hgeoaddress=std::make_unique<TH1F>("AmbGeoAddress:1d",         "Geographical address of roads",  64, -0.5, 63.5);
                m_hnhits=std::make_unique<TH1F>("AmbNHits:1d",             "Number of hits per spy buffer", 500, -0.5, 499.5);  

                //ML create all required StatusRegisterCollection objects here.
                m_statusRegisterAMBFactory=std::make_unique<StatusRegisterAMBFactory>(m_name + ".AMB", m_slot, "VHRC", m_isServerName, m_ipcpartition);
            }

            if (!m_AmbStandalone || (m_AuxStandalone && m_AmbStandalone))
            {
                try {
                    std::ostringstream oh_name;
                    if (m_slot < 10)
                    { 
                        oh_name << "FTK_PU-" << m_crate << "-0" << m_slot << "AUX_common";
                    }
                    else
                    { 
                        oh_name << "FTK_PU-" << m_crate << "-" << m_slot << "AUX_common";
                    }
                    //previously we were using getDFOHNameString, but format is wrong. Building the provider name using directly m_crate/m_slot
                    m_ohRawProviderAUX=std::make_shared<OHRawProvider<>>( m_ipcpartition , OHServer, oh_name.str());
                }
                catch ( daq::oh::Exception & ex)
                { // Raise a warning or throw an exception. 
                    daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
                    ers::warning(e);  
                }

                m_dfReaderAux=std::make_unique<DataFlowReaderAux>(m_ohRawProviderAUX, true);
                m_dfReaderAux->init(m_aux->name_ftk(),m_aux->get_clock_freqs(),m_aux->get_periods());

                try{
                    ERS_LOG("Making AUX StatusRegisterFactory");
                    m_statusRegisterAuxFactory=std::make_unique<StatusRegisterAuxFactory>(m_aux->name_ftk() , m_slot, "123456", m_isServerName, m_ipcpartition);
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in making AUX StatusRegisterFactory", ex);
                    ers::warning(e);
                }


            }

            tLog.end(ERS_HERE);  // End log conter

        }

        unsigned int countBits(unsigned int data) {
            int count=0;
            for (unsigned int i=0; i<sizeof(unsigned int)*8; i++) 
                if ( (data>>i)&0x1 ) count++;
            return count;
        }

        void ReadoutModule_PU::checkConfig(const daq::rc::SubTransitionCmd& cmd)
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
            try{
                m_aux->checkConfig();
            }
            catch(ers::Issue &ex){
                daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AUX firmware versions", ex);
                throw e;
            }
            tLog.end(ERS_HERE);
        }



        void ReadoutModule_PU::doConfigure(const daq::rc::TransitionCmd& cmd)
        { 
            ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
            ERS_LOG("ReadoutModule_PU::configure: SLOT = " << std::to_string(m_slot));  
            // In dryRun mode bypass all the vme commands
            if(m_dryRun)
            {
                ERS_LOG("ReadoutModule_PU: "  << cmd.toString() << " [dryRun] done");
                return;
            }
            //Configure AMB
            if (!m_AuxStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                ftkTimeLog tLog1(ERS_HERE, name_ftk(),"Configure AMB");
                try {
                    m_CanPublish = false;
                    m_AMB->RC_configure();
                }
                catch( daq::ftk::VmeError & err ) {
                    daq::ftk::VmeError e(ERS_HERE, "ReadoutModule_PU::configure: Error when initializing AmBoard at slot : " + m_slot );
                    ers::error(e);
                }
                tLog1.end(ERS_HERE);  // End log conter
            }

            if(!m_AmbStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                ftkTimeLog tLog1(ERS_HERE, name_ftk(),"Configure AUX");
                //Configure AUX
                try{
                    m_aux->configure();
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AUX configure", ex);
                    throw e;
                }
                tLog1.end(ERS_HERE);  // End log conter
            }
            tLog.end(ERS_HERE);  // End log conter
        }


        void ReadoutModule_PU::doUnconfigure(const daq::rc::TransitionCmd& cmd)
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());

            //AMB specific code
            ERS_LOG("ReadoutModule_PU::unconfigure: SLOT = " << std::to_string(m_slot));  

            if(m_dryRun)
            {
                ERS_LOG("ReadoutModule_PU: "  << cmd.toString() << " [dryRun] done");
                return;
            }
            if (!m_AuxStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                ftkTimeLog tLog1(ERS_HERE, name_ftk(),"Unconfigure AMB");
                try {
                    m_CanPublish = false;      
                    m_AMB->RC_unconfigure();
                }
                catch( daq::ftk::VmeError & err ) {
                    daq::ftk::VmeError e(ERS_HERE, "ReadoutModule_PU::configure: Error when initializing AmBoard at slot : " + m_slot );
                    ers::error(e);
                }
                tLog1.end(ERS_HERE);  // End log conter
            }
            if(!m_AmbStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                //AUX does nothing at unconfigure 
            }
            tLog.end(ERS_HERE);  // End log conter
        }



        void ReadoutModule_PU::doConnect(const daq::rc::TransitionCmd& cmd)
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());

            ERS_LOG("ReadoutModule_PU::connect: SLOT = " << std::to_string(m_slot));  
            // In dryRun mode bypass all the vme commands
            if(m_dryRun)
            {
                ERS_LOG("ReadoutModule_PU: "  << cmd.toString() << " [dryRun] done");
                return;
            }

            // If AUX and AMB, connect both
            if(!m_AmbStandalone & !m_AuxStandalone)
            {
                ftkTimeLog tLog1(ERS_HERE, name_ftk(),"Connect AUX");
                //connect AUX
                try{
                    m_aux->connect();
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AUX connect", ex);
                    throw e;
                }
                tLog1.end(ERS_HERE);  // End log conter

                //connect AMB
                ftkTimeLog tLog2(ERS_HERE, name_ftk(),"Connect AMB");
                try {
                    m_AMB->RC_connect();
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AMB connect", ex);
                    throw e;
                }
                tLog2.end(ERS_HERE);  // End log conter
            }

            // If only AUX, connect only AUX
            else if (!m_AmbStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                ftkTimeLog tLog1(ERS_HERE, name_ftk(),"Connect AUX");
                //connect AUX
                try{
                    m_aux->connect();
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AUX connect", ex);
                    throw e;
                }
                tLog1.end(ERS_HERE);  // End log conter
            }

            // If only AMB, connect only AMB
            else if (!m_AuxStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                ftkTimeLog tLog1(ERS_HERE, name_ftk(),"Connect AMB");
                //connect AMB
                try {
                    m_AMB->RC_connect();
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AMB connect", ex);
                    throw e;
                }
                tLog1.end(ERS_HERE);  // End log conter
            }

            tLog.end(ERS_HERE);  // End log conter
        }



        void ReadoutModule_PU::doDisconnect(const daq::rc::TransitionCmd& cmd)
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
            
            tLog.end(ERS_HERE);  // End log conter
        }


        void ReadoutModule_PU::startNoDF(const daq::rc::SubTransitionCmd& cmd)
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
            ERS_LOG("ReadoutModule_PU::startNoDF: SLOT = " << std::to_string(m_slot));  

            // In dryRun mode bypass all the vme commands
            if(m_dryRun)
            {
                ERS_LOG("ReadoutModule_PU: " << cmd.toString() << " [dryRun] done");
                return;
            }
            if (!m_AuxStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                ftkTimeLog tLogLocal(ERS_HERE, name_ftk(),"startNoDF AMB");
                // Reset histograms
                m_hlast_lvl1id->Reset();
                m_hnroads->Reset();
                m_hgeoaddress->Reset();
                m_hnhits->Reset();
                try{
                    m_AMB->RC_prepareForRun();
                }
                catch( daq::ftk::VmeError & err )
                {
                    throw daq::ftk::VmeError(ERS_HERE, "ReadoutModule_PU::configure: Error when initializing AmBoard at slot : " + m_slot );
                }
                tLogLocal.end(ERS_HERE);  // End log conter
            }
            if(!m_AmbStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                ftkTimeLog tLogLocal(ERS_HERE, name_ftk(),"startNoDF AUX");
                try{
                    ERS_LOG("AUX: Prepare for run");
                    m_aux->prepareForRun();
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AUX prepare for run", ex);
                    throw e;
                }
                tLogLocal.end(ERS_HERE);  // End log conter
            }
            tLog.end(ERS_HERE);  // End log conter
      }



        void ReadoutModule_PU::doPrepareForRun(const daq::rc::TransitionCmd& cmd)
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
            ERS_LOG("ReadoutModule_PU::prepareForRun: SLOT = " << std::to_string(m_slot));  
            m_CanPublish = true;
            tLog.end(ERS_HERE);  // End log conter
        }


        void ReadoutModule_PU::doStopRecording(const daq::rc::TransitionCmd& cmd)
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
            ERS_LOG("SLOT = " << std::to_string(m_slot));  
            m_CanPublish = false;

            ERS_LOG("Performing a last call to publish..");
            
            try{do_publish();}
            catch(ers::Issue &ex){
                daq::ftk::FTKMonitoringError e(ERS_HERE, name_ftk(), "Error in PU publish", ex);
                ers::error(e);
            }

            ERS_LOG("Stopping..");

            if (!m_AuxStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                ftkTimeLog tLogLocal(ERS_HERE, name_ftk(),"StopRecording AMB");
                try {
                    m_AMB->RC_stop();
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AMB stop", ex);
                    throw e;
                }
                tLogLocal.end(ERS_HERE);  // End log conter
            }

            if(!m_AmbStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                ftkTimeLog tLogLocal(ERS_HERE, name_ftk(),"StopRecording AUX");
                try{
                    m_aux->stop();
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AUX stop", ex);
                    throw e;
                }
                tLogLocal.end(ERS_HERE);  // End log conter
            }
            tLog.end(ERS_HERE);  // End log conter
        }


        void ReadoutModule_PU::doPublish( uint32_t flag, bool finalPublish )
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(),"Publish");
            if (!m_CanPublish) {
                ERS_LOG("Not in a good state yet, skipping");
                return;
            }
            if(m_dryRun)
            {
                // Left blank deliberately. Coded this way for consistency with the rest of the code.
            } 
            else
            { 
                m_CanPublish = false;
                try{do_publish();}
                catch(ers::Issue &ex){
                    daq::ftk::FTKMonitoringError e(ERS_HERE, name_ftk(), "Error in PU publish", ex);
                    ers::error(e);
                }
                m_CanPublish = true;
            }
            tLog.end(ERS_HERE);  // End log conter
        }


        void ReadoutModule_PU::doPublishFullStats( uint32_t flag )
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(),"PublishFullStat");

            //AMB specific code
            //ERS_LOG("ReadoutModule_PU::publishFullStats: SLOT = " << std::to_string(m_slot));  

            if (!m_CanPublish) {
                ERS_LOG("Not in a good state yet, skipping");
                return;
            }

            m_CanPublish = false;

            if (!m_AmbStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                ftkTimeLog tLogLocal(ERS_HERE, name_ftk(),"PublishFullStat AUX");
                try{
                    m_aux->publishFullStats(); 
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AUX publish full stats", ex);
                    ers::error(e);
                }
                tLogLocal.end(ERS_HERE);  // End log conter
            }

            if (!m_AuxStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                ftkTimeLog tLogLocal(ERS_HERE, name_ftk(),"PublishFullStat AMB");
                //++++++++++++++++++++++
                // Creation of the EventFragmentCollection that will be filled-up by the RC_publishFull function 
                //++++++++++++++++++++++
                std::vector<EventFragmentCollection*> eventCollectionsRoad;
                std::vector<EventFragmentCollection*> eventCollectionsHit;

                try {
                    m_AMB->RC_publishFull(eventCollectionsRoad, eventCollectionsHit);
                }
                catch(ers::Issue &ex) {
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AMB publish full stats", ex);
                    ers::error(e);
                }


                //++++++++++++++++++++++
                // 2.1 Read registers and fill monitoring variable
                //++++++++++++++++++++++
                m_ambslpNamed->ExampleString = "Test";
                m_ambslpNamed->ExampleU16    = 77;
                m_ambslpNamed->ExampleS32    = rand()%100;

                // retriving the DCDC information 

                vector<float> dcdcinfo;


                try{
                    ERS_LOG("AMB: DCDC read");
                    dcdcinfo = m_AMB->DCDC_read(false);
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AMB DCDC read", ex);
                    ers::error(e);
                }

                if (dcdcinfo.size() > 0) {

                    m_ambslpNamed->DCDC0VIn  = dcdcinfo[0];
                    m_ambslpNamed->DCDC0VOut = dcdcinfo[1];
                    m_ambslpNamed->DCDC0IOut = dcdcinfo[2];
                    m_ambslpNamed->DCDC0T1   = dcdcinfo[3];
                    m_ambslpNamed->DCDC0T2   = dcdcinfo[4];
                    m_ambslpNamed->DCDC1VIn  = dcdcinfo[5];
                    m_ambslpNamed->DCDC1VOut = dcdcinfo[6];
                    m_ambslpNamed->DCDC1IOut = dcdcinfo[7];
                    m_ambslpNamed->DCDC1T1   = dcdcinfo[8];
                    m_ambslpNamed->DCDC1T2   = dcdcinfo[9];
                    m_ambslpNamed->DCDC2VIn  = dcdcinfo[10];
                    m_ambslpNamed->DCDC2VOut = dcdcinfo[11];
                    m_ambslpNamed->DCDC2IOut = dcdcinfo[12];
                    m_ambslpNamed->DCDC2T1   = dcdcinfo[13];
                    m_ambslpNamed->DCDC2T2   = dcdcinfo[14];
                    m_ambslpNamed->DCDC3VIn  = dcdcinfo[15];
                    m_ambslpNamed->DCDC3VOut = dcdcinfo[16];
                    m_ambslpNamed->DCDC3IOut = dcdcinfo[17];
                    m_ambslpNamed->DCDC3T1   = dcdcinfo[18];
                    m_ambslpNamed->DCDC3T2   = dcdcinfo[19];
                }

                for(uint32_t i=0; i<eventCollectionsRoad.size(); i++) delete eventCollectionsRoad.at(i);
                for(uint32_t i=0; i<eventCollectionsHit.size(); i++) delete eventCollectionsHit.at(i);

                //++++++++++++++++++++++
                // 2.2 Pushish in IS  with schema (i.e.: properly)
                //++++++++++++++++++++++
                try { m_ambslpNamed->checkin();}
                catch ( daq::oh::Exception & ex)
                {  
                    daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
                    ers::warning(e);   
                }
                //++++++++++++++++++++++
                // 3 Publish the histograms
                //++++++++++++++++++++++

                // Publishing Histogram
                try 
                {
                    m_ohProvider->publish( *m_hlast_lvl1id, m_hlast_lvl1id->GetName(), true ); 
                    m_ohProvider->publish( *m_hnroads, m_hnroads->GetName(), true ); 
                    m_ohProvider->publish( *m_hgeoaddress, m_hgeoaddress->GetName(), true ); 
                    m_ohProvider->publish( *m_hnhits, m_hnhits->GetName(), true ); 
                }
                catch(ers::Issue &ex)
                {  
                    daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
                    ers::warning(e);   
                }
                tLogLocal.end(ERS_HERE);  // End log conter
            }

            m_CanPublish = true;
            tLog.end(ERS_HERE);  // End log conter
        }

        void ReadoutModule_PU::resynch(const daq::rc::ResynchCmd& cmd)
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
            
            tLog.end(ERS_HERE);  // End log conter
        }



        void ReadoutModule_PU::clearInfo() 
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(), "Clear histograms and counters");

            if (!m_AuxStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                // Reset histograms
                m_hlast_lvl1id->Reset();
                m_hnroads->Reset();
                m_hgeoaddress->Reset();
                m_hnhits->Reset();

                // Reset the IS counters
                m_ambslpNamed->ExampleS32 = 0 ;
                m_ambslpNamed->ExampleU16 = 0 ;
            }

            tLog.end(ERS_HERE);  // End log conter
        }

        void ReadoutModule_PU::do_publish()
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(),"Publish");

            //AMB
            if (!m_AuxStandalone || (m_AmbStandalone && m_AuxStandalone)) {
                ftkTimeLog tLogLocal(ERS_HERE, name_ftk(),"Publish AMB");

                try{m_statusRegisterAMBFactory->readout();}
                catch ( ers::Issue &ex)
                {  
                    daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
                    ers::warning(e);    
                }
                try{m_dfReaderAmb->readIS();}
                catch ( ers::Issue &ex)
                {  
                    daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
                    ers::warning(e); 
                }
                try{m_dfReaderAmb->publishAllHistos();}
                catch ( ers::Issue &ex)
                {  
                    daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
                    ers::warning(e);  
                }

                try {
                    ERS_LOG("AMB: Checking links");
                    m_AMB->RC_checkLinks();
                }
                catch(ers::Issue &ex) {
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AMB link check at publish", ex);
                    ers::error(e);
                }
                tLogLocal.end(ERS_HERE);  // End log conter
            }

            //AUX
            if(!m_AmbStandalone || (m_AmbStandalone && m_AuxStandalone))
            {
                ftkTimeLog tLogLocal(ERS_HERE, name_ftk(),"Publish AUX");
                try{m_statusRegisterAuxFactory->readout();}
                catch ( ers::Issue &ex)
                {  
                    daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
                    ers::warning(e); 
                }
                try{m_dfReaderAux->readIS();}
                catch ( ers::Issue &ex)
                {  
                    daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
                    ers::warning(e);  
                }
                try{m_dfReaderAux->publishAllHistos();}
                catch ( ers::Issue &ex)
                { 
                    daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
                    ers::warning(e);   
                }
                try{
                    ERS_LOG("AUX: Checking links");
                    m_aux->check_links();
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error in AUX link check at publish", ex);
                    ers::error(e); //don't want to fatal during running
                }
                tLogLocal.end(ERS_HERE);  // End log conter

            }
            tLog.end(ERS_HERE);  // End log conter
        }

        //FOR THE PLUGIN FACTORY
        extern "C"
        {
            extern ROS::ReadoutModule* createReadoutModule_PU();
        }

        ROS::ReadoutModule* createReadoutModule_PU()
        {
            return (new ReadoutModule_PU());
        }

    }   //namespace ftk

}   //namespace daq
