/********************************************************/
/*                                                      */
/********************************************************/

#ifndef READOUT_MODULE_PU_H
#define READOUT_MODULE_PU_H 

#include <string>
#include <vector>
#include <atomic>

//Common
#include "ipc/partition.h"
#include "ipc/core.h"
#include "is/info.h"
#include "oh/OHRootProvider.h"
#include "oh/OHRawProvider.h"
#include "RunControl/Common/OnlineServices.h"
#include "config/Configuration.h"
#include "DFdal/RCD.h"
#include "DFdal/ReadoutConfiguration.h"
#include "is/infoT.h"
#include "ProcessingUnit/dal/ReadoutModule_PU.h"
//AMB specific includes
#include "ftkcommon/dal/PatternBankConf.h"
#include "ftkcommon/Utils.h"
#include "ftkcommon/EventFragmentCollection.h"
#include "ftkcommon/SourceIDSpyBuffer.h"
#include "ftkcommon/FtkEMonDataOut.h" //Emon
#include "ftkdqm/EventFragmentAMBRoad.h"
#include "ftkdqm/EventFragmentAMBHit.h"
#include "ambslp/ambslp.h"
#include "ambslp/ambslp_amchip.h"
#include "ambslp/AMBoard.h"
#include "ambslp/ambslp_mainboard.h"
#include "ambslp/dal/ambslpNamed.h"
#include "ambslp/dal/AMBStandaloneTest.h"
#include "ambslp/dal/CHIP_config.h"
#include "ambslp/DataFlowReaderAMB.h"
#include "ambslp/StatusRegisterAMBFactory.h"
#include "ftkcommon/ReadoutModule_FTK.h"


//AUX includes
#include "aux/aux_card.h"
#include "aux/dal/auxNamed.h"
#include "aux/aux.h"

#include "aux/DataFlowReaderAux.h"
#include "aux/StatusRegisterAuxFactory.h"


namespace daq {
namespace ftk {

  class ReadoutModule_PU : public ReadoutModule_FTK
  {

  public: //Constructor and destructor

    ///Constructor (NB: executed at CONFIGURE transition)
    ReadoutModule_PU();

    ///Destructor (NB: executed at UNCONFIGURE transition)
    virtual ~ReadoutModule_PU() noexcept;

	public: //Overloaded methods inherited from ROS::ReadoutModule or ROS::IOMPlugin

    /// Set internal variables (NB: executed at CONFIGURE transition)
    virtual void setup(DFCountedPointer<ROS::Config> configuration) override;

    /// Reset internal statistics
    virtual void clearInfo() override;

    /// Get the list of channels connected to this ReadoutModule
    virtual const std::vector<ROS::DataChannel *> *channels() override;

    /// Set the Run Parameters
    //virtual void setRunConfiguration(DFCountedPointer<Config> runConfiguration);

    /// Get values of statistical counters
    // virtual DFCountedPointer<Config> getInfo();

    /// Get access to statistics stored in ISInfo class
    // virtual ISInfo* getISInfo();


  public: //Overloaded methods inherited from ReadoutModule_FTK

    /// RC configure transition
    virtual void checkConfig(const daq::rc::SubTransitionCmd& cmd) override;
    virtual void doConfigure(const daq::rc::TransitionCmd& cmd) override;

    /// RC connect transition: 
    virtual void doConnect(const daq::rc::TransitionCmd& cmd) override;

    /// RC start of run transition
    virtual void doPrepareForRun(const daq::rc::TransitionCmd& cmd) override;

    /// RC stop transition
    virtual void doStopDC(const daq::rc::TransitionCmd& cmd) override {}
    virtual void doStopROIB(const daq::rc::TransitionCmd& cmd) override {}
    virtual void doStopHLT(const daq::rc::TransitionCmd& cmd) override {}
    virtual void doStopRecording(const daq::rc::TransitionCmd& cmd) override; 
    virtual void doStopGathering(const daq::rc::TransitionCmd& cmd) override {} 
    virtual void doStopArchiving(const daq::rc::TransitionCmd& cmd) override {}
		/// RC startNoDF subtransition
    virtual void startNoDF(const daq::rc::SubTransitionCmd& cmd) override;

    /// RC unconfigure transition 
    virtual void doUnconfigure(const daq::rc::TransitionCmd& cmd) override;

    /// RC disconnect transition: 
    virtual void doDisconnect(const daq::rc::TransitionCmd& cmd) override;

    /// RC subtransition (IF NEEDED)
    // virtual void subTransition(const daq::rc::SubTransitionCmd& cmd) override;

    /// Method called periodically by RC
    virtual void doPublish( uint32_t flag, bool finalPublish ) override;

    /// Method called periodically with a longer interval by RC
    virtual void doPublishFullStats( uint32_t flag ) override;

    /// RC HW re-synchronization request
    virtual void resynch(const daq::rc::ResynchCmd& cmd);

	public: //RM specific public functions

    ///Method used to make available the RM name to ERS log
    inline std::string name_ftk() { return m_name; }

  private: //Common variables

    void do_publish();

    std::vector<ROS::DataChannel *>     m_dataChannels;
    DFCountedPointer<ROS::Config>       m_configuration;    /**< Configuration Object, Map Wrapper */
    std::string                         m_isServerName;     /**< IS Server				*/
    IPCPartition                        m_ipcpartition;     /**< Partition				*/
    uint32_t                            m_runNumber;        /**< Run Number				*/
    std::string                         m_name;							/**< RM name					*/
		std::string													m_appName;
    uint32_t                            m_slot;							/**< Slot number 			*/
    uint32_t                            m_crate;						/**< Slot number 			*/
    std::string                         m_manager;          ///UID of the Manager Application to which send the ACK
    std::unique_ptr<OHRootProvider>  		m_ohProvider;    	  ///< Histogram provider
    std::shared_ptr<OHRawProvider<>> 		m_ohRawProviderAMB; ///< Raw histogram provider
    std::shared_ptr<OHRawProvider<>> 		m_ohRawProviderAUX; ///< Raw histogram provider
    //std::unique_ptr<TH1F>           	m_histogram;

    // Histograms
    std::unique_ptr<TH1F>     					m_hlast_lvl1id;
    std::unique_ptr<TH1F>     					m_hnroads;
    std::unique_ptr<TH1F>     					m_hgeoaddress;
    std::unique_ptr<TH1F>     					m_hnhits;

	private: 
		//AMB specific Variables
    std::unique_ptr<ambslpNamed>        m_ambslpNamed;   		 ///< Access IS via schema
    std::unique_ptr<auxNamed>		        m_auxNamed;      		 ///< Access IS via schema
    bool                             		m_dryRun;        		 ///< Bypass VME calls
    std::unique_ptr<DataFlowReaderAMB> 	m_dfReaderAmb;   		 ///< DataFlowReaderAMB
    std::atomic<bool> 	    			m_CanPublish;
    std::unique_ptr<AMBoard> 						m_AMB;
    std::unique_ptr<StatusRegisterAMBFactory> m_statusRegisterAMBFactory;
    bool m_AmbStandalone; ///< Skip the AUX code to run AMB in standalone
    bool m_AuxStandalone; ///< Skip the AMB code to run AUX in standalone
    void reset_weak_config();


    //AUX stuff
    std::unique_ptr<daq::ftk::aux>  m_aux;                              /**< Implementation */
    unique_ptr<StatusRegisterAuxFactory> m_statusRegisterAuxFactory;
    std::unique_ptr<DataFlowReaderAux> m_dfReaderAux;   ///< DataFlowReaderAMB
    std::unique_ptr<TH1F>           m_Histogram;

  };

inline const std::vector<ROS::DataChannel *> *ReadoutModule_PU::channels()
	{	return &m_dataChannels; }

} // namespace ftk
} // namespace daq
#endif // READOUT_MODULE_PU_H
 
